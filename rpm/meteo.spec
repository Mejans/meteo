%global appname com.gitlab.bitseater.%{name}

Name:           meteo
Version:        0.9.8
Release:        1%{?dist}
Summary:        Forecast application using OpenWeatherMap API

License:        GPLv3+
URL:            https://gitlab.com/bitseater/meteo
Source0:        %{url}/-/archive/%{version}/%{name}-%{version}.tar.gz

%define DEPENS_RPM desktop-file-utils, rpmdevtools, git, gcc, wget, meson, ninja-build, vala, vala-devel, gtk3-devel, libsoup-devel, json-glib-devel, geocode-glib-devel, webkit2gtk3-devel, libappindicator-gtk3-devel, libappstream-glib

BuildRequires:  %{DEPENS_RPM}

%description
Know the forecast of the next hours & days.

Developed with Vala & Gtk, using OpenWeatherMap API.

Features:

- Current weather, with information about temperature, pressure, wind speed and
  direction, sunrise & sunset.
- Forecast for next 18 hours.
- Forecast for next five days.
- Choose your units (metric, imperial or british).
- Choose your city, with maps help.
- Awesome maps with weather info.
- System tray indicator.


%prep
%autosetup

%build
%meson
%meson_build

%install
%meson_install
%find_lang %{appname}

%check
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/%{appname}.appdata.xml
desktop-file-validate %{buildroot}%{_datadir}/applications/%{appname}.desktop

%files
%license COPYING
%doc README.md AUTHORS CREDITS.md CHANGELOG
%{_bindir}/%{appname}
%{_datadir}/applications/*.desktop
%{_datadir}/glib-2.0/schemas/*.gschema.xml
%{_datadir}/icons/hicolor/*/*/*.svg
%{_datadir}/locale/*/LC_MESSAGES/%{appname}.mo
%{_metainfodir}/*.appdata.xml
%{_mandir}/man1/*.1*

%changelog
* Sat Sep 28 2019 Carlos Suárez (bitseater)
- Indicator icon issue fixed.
- Fixed some lintian warnings on packaging.
- Fixed issues: #125, #128, #129.

