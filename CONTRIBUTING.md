## Contributing to METEO

METEO is a forecast application written in Vala and Gtk+. We are grateful for any contribution you can make.

## Reporting a bug

You can add a new issue to reporting a bug, ask for enhancements and new features on
[our issue section at Gitlab](https://gitlab.com/bitseater/meteo/issues).

## Translating Meteo

You can translate Meteo to your language going to [our Launchpad repo](https://translations.launchpad.net/bitseater.meteo), or simply [download the Meteo pot file](https://gitlab.com/bitseater/meteo/blob/master/po/com.gitlab.bitseater.meteo.pot), use your favorite editor (PoEdit, GTraslator,...) to create translation and send it opening a [Merge Request](https://gitlab.com/bitseater/meteo/merge_requests).

## Licensing

Contributions should be licensed under the GNU GPL v3.
