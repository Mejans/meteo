####Acknowledgements and credits:

- **Weather data**: Provided by [OpenWeatherMap](http://openweathermap.org/) under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) .
- **Maps images**: Provided by [Dark Sky](https://darksky.net) © The Dark Sky Company, LLC.
- **Images and backgrouds**: Provided by [Wikimedia Commons](https://commons.wikimedia.org)  and used as [free content](https://commons.wikimedia.org/w/index.php?title=Commons:Licensing&uselang=en) .
